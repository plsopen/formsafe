#!/bin/bash
# try to start flask from shell
# . venv/bin/activate
source venv/bin/activate
export FLASK_APP=app/formsafe.py
export FLASK_CONFIG=development
export FLASK_DEBUG=True
export FLASK_ENV=development
flask run --port 3000
