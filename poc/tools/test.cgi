#!/www/htdocs/w01a32ee/flask/test/venv/bin/python

# ! /usr/bin/python
# ! /home/username/virtualenv/application/x.y/bin/python

import sys
import os

# activate_this = "/home/username/virtualenv/application/x.y/bin/activate_this.py"
# activate_this = '/www/htdocs/w01a32ee/flask/test/venv/bin/activate_this.py'
# because activate_this.py is missing from venv created with python3
# sys.path.insert(0, '/home/<username>/public_html/cgi-bin/myenv/lib/python2.7/site-packages')
sys.path.insert(0, '/www/htdocs/w01a32ee/flask/test/venv/lib/python3.6/site-packages')

# in order to import the app:
sys.path.append(os.path.abspath('/www/htdocs/w01a32ee/flask/test'))

# you can now put simple.py in the above path
from simple import app
from wsgiref.handlers import CGIHandler

CGIHandler().run(app)

# below works for testing without app import or flask
# print("Content-type:text/html\r\n\r\n")
# print('<html>')
# print('<head>')
# print('<title>Virtualenv test</title>')
# print('</head>')
# print('<body>')
# print('<h3>If you see this, the module import was successful</h3>')
# print(sys.version)
# print('</body>')
# print('</html>')
