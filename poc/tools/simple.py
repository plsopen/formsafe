from flask import Flask
import os

# Flask application settings.
DEBUG = bool(os.environ.get('DEBUG'))
SECRET_KEY = 'secret - change me'  # TODO: change me.

app = Flask(__name__)


@app.route('/')
def index():
    return 'hello'


@app.route('/a')
def a():
    return 'a'


if __name__ == '__main__':
    app.run()  # Use Flask's builtin WSGI server.
