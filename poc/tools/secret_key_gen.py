
# call like: python secret_key_gen.py
# make sure local.cfg defines:
# SECRET_KEY
# WTF_CSRF_SECRET_KEY
import os
import binascii
print(binascii.hexlify(os.urandom(24)))
# 'e328f5c0cc8e32c8c2ae5e214c52394d95449a851e5c0b1b'
