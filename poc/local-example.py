SECRET_KEY = 'secret string'

WTF_CSRF_ENABLED = True
WTF_CSRF_CHECK_DEFAULT = True
WTF_CSRF_SECRET_KEY = 'another secret string'
# Max age in seconds for CSRF tokens. Default is 3600. If set to None, the CSRF token is valid for the life of the session.
WTF_CSRF_TIME_LIMIT = 60 * 60

# SESSION_COOKIE_SECURE = True
# REMEMBER_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
REMEMBER_COOKIE_HTTPONLY = True

PRIVATEKEYPEM = """-----BEGIN EC PRIVATE KEY-----key-here-----END EC PRIVATE KEY-----"""

PUBLICKEYPEM = """-----BEGIN PUBLIC KEY-----key-here-----END PUBLIC KEY-----"""
