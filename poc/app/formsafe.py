import os
import json
from flask import Flask, render_template, request, Response
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from flask_wtf.csrf import CSRFProtect, CSRFError

basedir = os.path.abspath(os.path.dirname(__file__))

STATIC_URL_PATH = '/'
STATIC_FOLDER = 'static'

app = Flask(__name__,
            static_url_path=STATIC_URL_PATH,
            static_folder=STATIC_FOLDER)

# don't use instance_path, use one directory above
app.config.from_pyfile(os.path.join(basedir, '..', 'instance', 'local.py'), silent=False)

# on production, have better security:
if not app.debug:
    # don't use print in cgi
    # print('not app.debug')
    app.config.update(
        SESSION_COOKIE_SECURE=True,
        SESSION_COOKIE_HTTPONLY=True,
        # SESSION_COOKIE_SAMESITE='None'
        # SESSION_COOKIE_SAMESITE = 'Lax'
        SESSION_COOKIE_SAMESITE='Strict'
    )

csrf = CSRFProtect(app)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500


@app.errorhandler(CSRFError)
def handle_csrf_error(CSRFError):
    resp = 'Unfortunately the request is invalid (CSRF error detected).'
    return Response(resp, status=403, mimetype='text/plain')


@app.route('/', methods=['GET'])
@app.route('/index.html', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/s')
def s():
    return render_template('survey/compsurvey.html',
                           public_key=app.config['PUBLICKEYPEM'],
                           # private_key=app.config['PRIVATEKEYPEM']
                           private_key='NOT PROVIDED'
                           )


@app.route('/surveyintro.json')
def surveyintro_json():
    response = app.response_class(
        response=render_template('survey/surveyintro.json', answers_required='true', survey_title='FormSafe JWE'),
        mimetype='application/json'
    )
    return response


@app.route('/surveyecho', methods=['GET', 'POST'])
def surveyecho():
    res = request.get_json()
    res_json = json.dumps(res)
    return res_json


@app.route('/decrypt', methods=['GET'])
def decrypt():
    encrypted = request.values.get('response')
    return render_template('decrypt.html',
                           # public_key=app.config['PUBLICKEYPEM'],
                           public_key='NOT PROVIDED',
                           private_key=app.config['PRIVATEKEYPEM'],
                           encrypted=encrypted)
